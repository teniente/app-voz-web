let sr = new webkitSpeechRecognition();
sr.lang = "es-MX";
let synth = window.speechSynthesis;
let voices = synth.getVoices();

let mostrarTextoEscuchado = document.getElementById("mostrarTextoEscuchado");

sr.onresult = (resultado)=>{
    let indiceUltimoElemento = resultado.results.length - 1;
    let textoEscuchado = resultado.results[indiceUltimoElemento][0].transcript.toLowerCase();

    let patronesYoutube = [
        "buscar en youtube",
        "puedes buscar en youtube",
        "busca en youtube",
        "por favor busca en youtube"
    ];

    let patronesGoogle = [
        "buscar en google",
        "puedes buscar en google",
        "busca en google",
        "por favor busca en google"
    ];

    if(textoEscuchado.includes("hola mi nombre es")){
        mostrarTextoEscuchado.innerHTML = "hola "+textoEscuchado.split("hola mi nombre es")[1]+" es un gusto conocerte";
        speak("hola "+textoEscuchado.split("hola mi nombre es")[1]+" es un gusto conocerte");
    }

    if(textoEscuchado.includes(patronesYoutube[0])){
        console.log("buscando en youtube"+ textoEscuchado.split(patronesYoutube[0])[1]);
        openURL("https://www.youtube.com/results?search_query=",textoEscuchado.split(patronesYoutube[0])[1]);
        speak("buscando en youtube"+ textoEscuchado.split(patronesYoutube[0])[1]);

    }
    if(textoEscuchado.includes(patronesGoogle[0])){
        console.log("buscando en google"+ textoEscuchado.split(patronesGoogle[0])[1]);
        openURL("http://www.google.com/search?q=",textoEscuchado.split(patronesGoogle[0])[1]);
        speak("buscando en google"+ textoEscuchado.split(patronesGoogle[0])[1]);
    }

};

function speak(texto){
    let utterance = new SpeechSynthesisUtterance(texto);
    utterance.voice = voices.filter(
        function (voice) {
            return voice.name == 'Monica';
        })[0];
    window.speechSynthesis.speak(utterance);
}

function openURL( url , texto ){
    window.open( url+texto , "_blank" );
}

sr.start();
